#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

from keras.utils import Sequence
import numpy as np
import os, glob

# Global variables to share preloaded image dataset between TripletGenerator and ImageGenerator.
# This prevents storing the same data twice in case both classes are created with preload=True.
# Trick from: https://stackoverflow.com/a/9473485
data_images = None
data_ages   = None

class TripletGenerator(Sequence):

    def __init__(self,
                 dataframe,
                 distance_matrices,
                 batch_size,
                 data_folder,
                 augmentation=True,
                 positive_quantile=0.05,
                 negative_quantile=0.50,
                 negative_quantile_max=None,
                 imsize=(128, 256),
                 verbose=False,
                 preload=True):

        self.dataframe = dataframe
        self.distance_matrices = distance_matrices
        for dm in self.distance_matrices:
            if dm.ndim != 2:
                raise ValueError('Distance matrix must be 2D')
            if dm.shape[0] != dm.shape[1]:
                raise ValueError('Distance matrix must be square')
        self.pq = positive_quantile
        self.nq = negative_quantile
        self.nq_max = negative_quantile_max
        if self.nq_max is not None:
            if self.nq_max <= self.nq:
                raise ValueError(f'negative_quantile_max ({self.nq_max}) must be greater than negative_quantile ({self.nq})')
        self.data_folder = data_folder
        self.batch_size = batch_size
        self.augmentation = augmentation
        self.h, self.w = imsize
        self.verbose = verbose
        self.preload = preload
        if self.preload:
            self.preload_images()

    def __repr__(self):
        str  = f'Image folder : {self.data_folder}\n'
        str += f'Length of data frame: {len(self.dataframe)}\n'
        str += f'Number of distance matrices: {len(self.distance_matrices)}\n'
        str += f'Positive quantile: {self.pq}\n'
        str += f'Negative quantile: {self.nq}\n'
        str += f'Negative quantile (maximum): {self.nq_max}\n'
        str += f'Augmentation: {self.augmentation}\n'
        str += f'Image size: {self.h, self.w}'
        return str

    def __str__(self):
        self.__repr__()

    def __len__(self):
        return int(np.ceil(len(self.dataframe) / float(self.batch_size)))

    def __getitem__(self, idx, return_ages=False):

        # get number of triplets
        ## note: this list comprehesion that filters out Nones returned by get_triplet()
        batch = [triplet for triplet in (self.get_triplet() for _ in range(self.batch_size)) if triplet is not None ]
        batch_x = np.array([triplet[0] for triplet in batch])
        #batch_x = np.array([triplet for triplet in (self.get_triplet() for _ in range(self.batch_size)) if triplet is not None ])
        batch_x = np.moveaxis(batch_x, 0, 1)
        batch_x = list(batch_x) # ndarray.tolist() creates nested list
        if len(batch_x[0]) != self.batch_size:
            print(f'Warning: batch size = {len(batch_x[0])}')

        # get fake targets (unused but necessary)
        batch_y = None
        if return_ages:
            batch_y = np.array([triplet[1] for triplet in batch]) # return ages

        return batch_x, batch_y

    def preload_images(self):
        global data_images
        global data_ages
        if data_images is None and data_ages is None: # if other class did not yet preload dataset
            from utils import read_images
            filenames = self.dataframe['Filename'].values
            print(f'Preloading {len(filenames)} images into memory...')
            self.data_images = data_images = read_images(filenames, self.h, self.w, image_folder=self.data_folder)
            self.data_ages = data_ages = self.dataframe['Age'].values
        else:
            print(f'Data already preloaded... Sharing dataset.')
            self.data_ages = data_ages
            self.data_images = data_images

    def read_image(self, path):
        from utils import read_image
        return read_image(path, self.h, self.w, image_folder=self.data_folder)

    def index_to_filename(self, index):
        if index < 0 or index >= len(self.dataframe):
            raise ValueError(f'non-valid index: {index} (len(self.dataframe) = {len(self.dataframe)})')
        return self.dataframe.iloc[index]['Filename']

    def get_reference(self):
        reference_index = np.random.choice(range(len(self.dataframe)))
        return reference_index

    def get_positive(self, distances):
        # get the positive (near) quantile
        near_q = np.quantile(distances, self.pq)
        # get the indices of all samples closer than the near_q
        indices = np.nonzero(distances < near_q)[0]
        if len(indices) == 0:
            return None
        if self.verbose: print(f'positive indices = {len(indices)}')
        # get a random index from the close samples
        positive_index = np.random.choice(indices)
        return positive_index

    def get_negative(self, distances):
        # get the negative (far) quantile
        far_q = np.quantile(distances, self.nq)
        if self.nq_max is None:
            # get the indices of all samples larger than the far_q, but close than 2 * far_q
            indices = np.nonzero(distances > far_q)[0]
        else: # if a maximum negative quantile is given
            # get the maximum negative (far) quantile
            far_q_max = np.quantile(distances, self.nq_max)
            # get the indices of all samples larger than the far_q, but close than 2 * far_q
            indices = np.nonzero( (distances > far_q) & (distances < far_q_max))[0]
        if len(indices) == 0:
            return None
        if self.verbose: print(f'negative indices = {len(indices)}')
        # get a random index from the close samples
        negative_index = np.random.choice(indices)
        return negative_index


    def get_triplet(self):
        # get index of random reference image
        reference_index = self.get_reference()

        # select random type of distance
        metric = np.random.choice(range(len(self.distance_matrices)))
        distances = self.distance_matrices[metric][reference_index]

        # get index of random reference image
        positive_index = self.get_positive(distances)
        negative_index = self.get_negative(distances)
        triplet_indices = [reference_index, positive_index, negative_index]
        if None in triplet_indices: return None

        if self.preload: # read data from memory
            # get image
            triplet_ims = self.data_images[triplet_indices]
            # get data
            triplet_ages = self.data_ages[triplet_indices]
        else: # read data from disc
            # get filenames for triplets
            triplet_fns = [self.index_to_filename(ind) for ind in triplet_indices]
            if None in triplet_fns: return None
            if self.verbose: print(triplet_fns)

            # read images
            triplet_ims = np.array([self.read_image(fn) for fn in triplet_fns])
            triplet_ages = self.dataframe['Age'].iloc[triplet_indices].values
            if self.verbose:
                print('Indices: ', triplet_indices)
                print('Ages:    ', triplet_ages)

        # data augmentation
        if self.augmentation:
            # each image has 50% probability to get horizontally flipped
            triplet_ims = np.array([np.fliplr(im) if np.random.random() < 0.5 else im for im in triplet_ims])

        return triplet_ims, triplet_ages


    def plot_batch(self):

        import matplotlib.pyplot as plt
        batch = self.__getitem__(0, return_ages=True)
        images = np.moveaxis(batch[0],0,1)
        ages   = batch[1]
        for triplet_ims, triplet_ages in zip(images, ages):
            # plot the images
            fig, ax = plt.subplots(1,3,figsize=(12,6))
            ax[0].imshow(np.squeeze(triplet_ims[0]), cmap='gray'); ax[0].set_title(f'Reference, age = {triplet_ages[0]/12:.1f} y')
            ax[1].imshow(np.squeeze(triplet_ims[1]), cmap='gray'); ax[1].set_title(f'Positive, age = {triplet_ages[1]/12:.1f} y')
            ax[2].imshow(np.squeeze(triplet_ims[2]), cmap='gray'); ax[2].set_title(f'Negative, age = {triplet_ages[2]/12:.1f} y')
            for a in ax: a.axis('off')
            plt.show()
            plt.close()

#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

class ImageGenerator(Sequence):

    def __init__(self,
                 dataframe,
                 batch_size,
                 data_folder,
                 augmentation=True,
                 imsize=(128, 256),
                 verbose=False,
                 preload=True):

        self.dataframe = dataframe
        self.data_folder = data_folder
        self.batch_size = batch_size
        self.augmentation = augmentation
        self.h, self.w = imsize
        self.verbose = verbose
        self.preload = preload
        if self.preload:
            self.preload_images()

    def __repr__(self):
        str  = f'Image folder : {self.data_folder}\n'
        str += f'Length of data frame: {len(self.dataframe)}\n'
        str += f'Augmentation: {self.augmentation}\n'
        str += f'Image size: {self.h, self.w}'
        return str

    def __str__(self):
        self.__repr__()

    def __len__(self):
        return int(np.ceil(len(self.dataframe) / float(self.batch_size)))

    def __getitem__(self, idx, return_ages=False):

        # get number of triplets
        ## note: this list comprehesion that filters out Nones returned by get_sample()
        batch = [sample for sample in (self.get_sample() for _ in range(self.batch_size)) if sample is not None ]
        batch_x = np.array([sample[0] for sample in batch])

        # get fake targets (unused but necessary)
        batch_y = None
        if return_ages:
            batch_y = np.array([sample[1] for sample in batch]) # return ages

        return batch_x, batch_y

    def preload_images(self):
        global data_images
        global data_ages
        if data_images is None and data_ages is None: # if other class did not yet preload dataset
            from utils import read_images
            filenames = self.dataframe['Filename'].values
            print(f'Preloading {len(filenames)} images into memory...')
            self.data_images = data_images = read_images(filenames, self.h, self.w, image_folder=self.data_folder)
            self.data_ages = data_ages = self.dataframe['Age'].values
        else:
            print(f'Data already preloaded... Sharing dataset.')
            self.data_ages = data_ages
            self.data_images = data_images


    def read_image(self, path):
        from utils import read_image
        return read_image(path, self.h, self.w, image_folder=self.data_folder)

    def index_to_filename(self, index):
        if index < 0 or index >= len(self.dataframe):
            raise ValueError(f'non-valid index: {index} (len(self.dataframe) = {len(self.dataframe)})')
        return self.dataframe.iloc[index]['Filename']

    def get_reference(self):
        reference_index = np.random.choice(range(len(self.dataframe)))
        return reference_index

    def get_sample(self):
        # get index of random reference image
        reference_index = self.get_reference()


        # read data
        if self.preload:
            # get image
            im = self.data_images[reference_index]
            # get data
            age = self.data_ages[reference_index]
        else:
            # get filename
            fn = self.index_to_filename(reference_index)
            # get image
            im = self.read_image(fn)
            # get age
            age = self.dataframe['Age'].iloc[reference_index]

        # data augmentation
        if self.augmentation:
            # each image has 50% probability to get horizontally flipped
            im = np.fliplr(im) if np.random.random() < 0.5 else im

        return im, age


    def plot_batch(self):

        import matplotlib.pyplot as plt
        batch = self.__getitem__(0, return_ages=True)
        images = batch[0]
        ages   = batch[1]

        nc = 5
        nr = len(images) // nc  + 1
        fig, ax = plt.subplots(nr, nc, figsize=(12,2*nr))
        ax = ax.flatten()
        for i, (im, age) in enumerate(zip(images, ages)):
            # plot the images
            ax[i].imshow(np.squeeze(im), cmap='gray');
            ax[i].set_title(f'Age = {age/12:.1f} y')
            ax[i].axis('off')
        for a in ax[i+1:]: a.set_visible(False)


