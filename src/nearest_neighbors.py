from keras.models import Model
import pandas as pd
import numpy as np
import warnings
from tqdm import tqdm_notebook as tqdm

class NearestNeighbor():


    def __init__(self, model:Model,
                       df:pd.DataFrame,
                       image_folder:str,
                       h:int,
                       w:int):
        # initialize
        if not isinstance(model.model, Model):
            raise ValueError(f'model.model must be an instance of keras.models.Model, but found {type(model)}.')
        else:
            self.model = model
        if not hasattr(self.model, 'latent_dim'):
            raise ValueError(f'model must be have attribute `latent_dim`.')
        transform_op = getattr(self.model, 'transform', None)
        if not callable(transform_op):
            raise ValueError(f'model must be have function `transform()`.')

        required_column_names = ['Filename', 'Age']
        if not all(colname in df.columns for colname in required_column_names):
            raise ValueError(f'Dataframe must contain columns: {required_column_names}')
        self.df = df
        self.image_folder = image_folder
        self.h = h
        self.w = w
        self.neighbors = None # created in fit()
        self.embeddings = None # created in get_embeddings()
        self.emb_umap = None # created in umap_embedding()

    def __repr__(self):
        return str(self.__dict__)

    def get_embeddings(self, chunk_size:int=256):
        '''Compute embeddings of items in `df` using train `self.model`.
        `df` is assumed to be a DataFrame with columns `Filename` and `Age`.
        '''
        from utils import read_images
        fns = self.df['Filename'].values
        num_chunks = len(fns) // chunk_size
        self.embeddings = np.zeros([0,self.model.latent_dim])
        # - read images in chunks
        for fns_chunk in tqdm(np.array_split(fns, num_chunks), total=num_chunks):
            ims = read_images(fns_chunk,
                                h = self.h,
                                w = self.w,
                                image_folder = self.image_folder,
                                progressbar=False)
            # - for each chunk, run forward pass through model
            output = self.model.transform(ims)
            self.embeddings = np.append(self.embeddings, output, axis=0)

        # store embeddings in df (note, not used!)
        self.df['Embedding'] = list(self.embeddings)

    def fit(self, **kwargs):
        '''
        Args:
            as in sklearn.neighbors.NearestNeighbors:
            https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.NearestNeighbors.html
            e.g. n_neighbors=128, radius=5.0, algorithm='auto', metric='euclidean'
        '''
        if self.embeddings is None:
            warnings.warn('Calculating embeddings...')
            self.get_embeddings()
        from sklearn.neighbors import NearestNeighbors
        #neighbors = NearestNeighbors(n_neighbors=128, radius=5.0, algorithm='auto', metric='euclidean')
        self.neighbors = NearestNeighbors(**kwargs)
        self.neighbors.fit(self.embeddings)


    def get_knn(self, query_image_fn:str, k:int=100, query_age_true:float=None, plot:bool=False):
        '''Get k nearest neighbors given a query image

        Args:
            query_image_fn: str
                filename of query image
            k: int
                number of neighbors to retrieve
            query_age_true: float
                true age of query image (in months), if known
            plot: bool
                whether or not to plot

        Returns:
            age_mean: float
                mean age of retrieved images (in years)
            age_std: float
                standard deviation of ages of retrieved images (in years)
        '''

        # read image and get its embedding
        from utils import read_image
        query_image = read_image(query_image_fn,
                                    h=self.h, w=self.w,
                                    image_folder=self.image_folder)
        # add batch dimension
        query_image = query_image[np.newaxis, ...]
        embedding_query = self.model.transform(query_image)

        # get indices of k nearest neighbors
        distances, nbs_indices = self.neighbors.kneighbors(embedding_query,
                                                            n_neighbors=k,
                                                            return_distance=True)
        distances = distances[0][1:]
        nbs_indices = nbs_indices[0][1:]

        # get ages of nearest neighbors
        ages = self.df['Age'].iloc[nbs_indices]  # in months
        age_mean = np.mean(ages/12)  # in years
        age_median = np.median(ages/12)  # in years
        age_std = np.std(ages/12)  # in years

        # probability
        probability = None
        if query_age_true is not None:
            import scipy.stats as stats
            mu = age_mean
            sigma = age_std
            probability = stats.norm(mu, sigma).pdf(query_age_true/12)
            #print(f'mu = {mu:.1f}, sigma = {sigma:.1f}, query_age_true = {query_age_true/12:.1f} => p = {probability:.3f}')


        if plot:
            self._plot_knn(query_image=query_image,
                            nbs_indices=nbs_indices,
                            distances=distances,
                            query_age_true=query_age_true)

        return age_mean, age_median, age_std, probability


    def _plot_knn(self, query_image, nbs_indices, distances, query_age_true:float=None, plot_neighbors:bool=True):
        '''Plot query image and retrieved k nearest neighbors (to be called from get_knn())'''

        # get knn filenames and read images
        from utils import read_images
        ims_filenames = self.df['Filename'].iloc[nbs_indices].values
        ims = read_images(ims_filenames,
                            h=self.h, w=self.w,
                            image_folder=self.image_folder)

        # get ages
        ages = self.df['Age'].iloc[nbs_indices].values  # in months
        age_mean = np.mean(ages/12)  # in years
        age_median = np.median(ages/12)  # in years
        age_std = np.std(ages/12)  # in years

        # plot reference image and histogram
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(1,2,figsize=(12,4))
        ax[0].imshow(np.squeeze(query_image), cmap='gray');
        ax[0].axis('off')
        if query_age_true == None:
            ax[0].set_title(f'Query image')
        else:
            ax[0].set_title(f'Query image, true age = {query_age_true/12:.1f} years')

        # plot histogram
        ax[1].hist(ages/12, bins=10)
        ax[1].set_title(f'Ages, mean = {age_mean:.1f} +/- {age_std:.1f} years (med = {age_median:.1f})')
        ax[1].set_xlim([0,30])
        ax[1].set_xlabel('Age (years)')
        ax[1].set_ylabel('Frequency')
        if query_age_true != None:
            ax[1].axvline(x=query_age_true/12, linestyle='-.', color='k', linewidth=2, zorder=10)

        # plot normal distribution with estimated mean and std
        import scipy.stats as stats
        mu = age_mean
        sigma = age_std
        ax[1] = ax[1].twinx() # second y-axis
        x = np.linspace(*ax[1].get_xlim(), 100)
        ax[1].plot(x, stats.norm.pdf(x, mu, sigma), color='g', lw=2)
        ax[1].set_ylabel('Probability')

        if plot_neighbors:
            # plot neighbors
            #nc = nr = int(np.ceil(np.sqrt(len(ims))))
            nc = 6
            nr = len(ims)//nc + 1
            fig, ax = plt.subplots(nr, nc, figsize=(24,2*nr))
            ax = ax.flatten()
            for i, im in enumerate(ims):
                ax[i].imshow(np.squeeze(im), cmap='gray')
                ax[i].axis('off')
                ax[i].set_title(f'age = {ages[i]/12:.1f}   d = {distances[i]:.2f}')
            for a in ax[i+1:]: a.set_visible(False)

        plt.show()

    # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    def plot_latents(self):

        if self.embeddings is None:
            raise ValueError(f'Embedding not calculated yet. Please run get_embeddings() or fit() first.')
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(1,1,figsize=(6,4))
        std_latents = self.embeddings.std(axis=0)
        ax.bar(range(std_latents.shape[0]), std_latents)
        ax.set_title('standard deviations of latents')
        ax.set_xlabel('latents')
        ax.set_ylabel('std')

    def umap_embedding(self, n_components=2, seed=42):
        try:
            import umap
        except:
            raise ImportError(f'Module `umap` not found. Please install using `pip install umap-learn`')
        # UMAP dimensionality reduction to 2D
        self.emb_umap = umap.UMAP(n_components=n_components, random_state=seed).fit_transform(self.embeddings)


    def plot_embedding(self, size:float=20, color:str='Age',
                            n_components:int=2, images:bool=False,
                            max_num_images:int=1024, seed:int=42):
        '''Plot UMAP embedding

        Args:
            size: float
                marker size (e.g. 10) or image zoom factor (e.g. 0.1)
        '''

        if self.emb_umap is None or self.emb_umap.shape[-1] != n_components:
            self.umap_embedding(n_components=n_components, seed=seed)
        # color
        colors = self.df[color]

        if self.emb_umap.shape[-1] == 2:
            if not images:
                # 2D scatter plot
                import matplotlib.pyplot as plt
                fig, ax = plt.subplots(1,1,figsize=(10,10))
                ax.scatter(*self.emb_umap.T, s=size, c=colors)
                #ax.axis('equal')
                ax.set_aspect('equal', 'box')
                ax.set_xlabel('UMAP 1')
                ax.set_ylabel('UMAP 2')
                ax.set_title(f'UMAP embedding (color = {color})')
            else:
                # 2D scatter plot with images, possibly with maximum number of images
                from utils import read_images
                if len(self.df) > max_num_images:
                    indices = np.random.choice(max_num_images, size=max_num_images)
                    warnings.warn(f'Plotting only `max_num_images`={max_num_images} number of images.')
                else:
                    indices = range(max_num_images)
                images = read_images(self.df['Filename'].iloc[indices], h=self.h, w=self.w, image_folder=self.image_folder)
                self._scatter_images(positions=self.emb_umap[indices], images=images, zoom=size)
        # 3D interactive plot in case of 3 components
        elif self.emb_umap.shape[-1] == 3:
            import ipyvolume as ipv
            ipv.quickscatter(*self.emb_umap.T, size=1, marker="sphere")
            ipv.show()

    def _scatter_images(self, positions, images, zoom=0.1, figsize=(20,20)):
        import matplotlib.pyplot as plt
        from matplotlib.offsetbox import OffsetImage, AnnotationBbox
        fig, ax = plt.subplots(figsize=figsize)
        x, y = positions[:,0], positions[:,1]
        ax.scatter(x,y)

        for x0, y0, img in zip(x, y, images):
            img = np.squeeze(img)
            img = OffsetImage(img, zoom=zoom, cmap='gray')
            ab = AnnotationBbox(img, (x0, y0), xycoords='data', frameon=False)
            ax.add_artist(ab)

