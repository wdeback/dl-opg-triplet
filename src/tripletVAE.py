from keras import backend as K
from utils import limit_GPU_memory
import numpy as np
import warnings

class TripletVAE(object):

    def __init__(self,
                 input_shape:tuple,
                 latent_dim:int=64,
                 recon_loss:str='bce',
                 beta:float=10.0,
                 gamma:float=1.0,
                 margin:float=10.0,
                 optimizer:str='nadam',
                 n_filters:int=32,
                 n_layers:int=3,
                 kernel_size:int=3):

        '''Triplet VAE network

        Args:
            input_shape: tuple
                shape of input images, excluding batch size (H,W,C)
            latent_dim: int
                length of latent vector
            beta: float
                weight of VAE-KL loss, relatively to VAE reconstruction loss
            gamma: float
                weight of triplet loss
            margin: float:
                margin (hinge) in triplet loss
            optimizer: str
                SGD optimizer
        '''
        self.input_shape = input_shape
        self.latent_dim = latent_dim
        self.beta = beta
        self.gamma = gamma
        self.margin = margin
        self.n_filters = n_filters
        self.n_layers = n_layers
        self.kernel_size = kernel_size

        valid_recon_losses = ['bce', 'xe', 'crossentropy', 'mse', 'mean_square_error']
        if recon_loss not in valid_recon_losses:
            warnings.warn(f'recon_loss must be one of: {valid_recon_losses}')
        elif recon_loss in ['bce', 'xe', 'crossentropy']:
            self.reconstruction_loss_crossentropy = True # crossentropy
        else:
            self.reconstruction_loss_crossentropy = False # MSE
        self.optimizer = optimizer
        self.submodel = None
        limit_GPU_memory()  # allow VRAM growth
        self.model = self.build_model()
        self.plot()
        self.history = None


    def get_VAE(self):
        from betaVAE import BetaVAE
        betavae = BetaVAE(input_shape=self.input_shape,
                            latent_dim=self.latent_dim,
                            beta=10.0,
                            n_layers=self.n_layers,
                            n_filters=self.n_filters,
                            kernel_size=self.kernel_size,
                            compile=False)
        return betavae.encoder, betavae.decoder, betavae.model


    def build_model(self):
        from keras import layers, models, metrics
        import keras.backend as K

        # create one CNN model
        self.encoder, self.decoder, self.autoencoder = self.get_VAE()

        # images
        input_image_r = layers.Input(shape=self.input_shape, name='reference_image')
        input_image_p = layers.Input(shape=self.input_shape, name='positive_image')
        input_image_n = layers.Input(shape=self.input_shape, name='negative_image')

        encoding_r, encoding_logvar_r, reconstr_r = self.autoencoder(input_image_r)
        encoding_p, encoding_logvar_p, reconstr_p = self.autoencoder(input_image_p)
        encoding_n, encoding_logvar_n, reconstr_n = self.autoencoder(input_image_n)

        # mean encoding output
        output_encoding = layers.Concatenate(axis=-1)([encoding_r,
                                                       encoding_p,
                                                       encoding_n])
        output_encoding = layers.Activation('linear')(output_encoding)
        output_encoding = layers.Reshape( (-1,3) )(output_encoding)

        # logsigma of encoding output
        output_encoding_logvar = layers.Concatenate(axis=-1)([encoding_logvar_r,
                                                              encoding_logvar_p,
                                                              encoding_logvar_n])
        output_encoding_logvar = layers.Activation('linear')(output_encoding_logvar)
        output_encoding_logvar = layers.Reshape( (-1,3) )(output_encoding_logvar)

        # reconstruction output
        output_reconstr = layers.Concatenate(axis=-1)([reconstr_r,
                                                       reconstr_p,
                                                       reconstr_n])
        output_reconstr = layers.Activation('linear')(output_reconstr)
        output_encoding = layers.Reshape( (-1,3) )(output_encoding)

        # model input/output
        model = models.Model(inputs=[input_image_r,
                                     input_image_p,
                                     input_image_n],
                             outputs=[output_encoding,
                                      output_encoding_logvar,
                                      output_reconstr],
                             name='triplet_vae_network')

        # LOSSES: reconstruction loss + beta * KL loss + triplet loss

        # ## Reconstruction loss (MSE or BCE) ##
        # def reconstruction_loss_item(inp, outp):
        #     h, w = self.input_shape[1:3]
        #     if self.reconstruction_loss_crossentropy:
        #         loss = h * w * metrics.binary_crossentropy(K.flatten(inp), K.flatten(outp))
        #     else: # mean square error
        #         loss = h * w * metrics.mse(K.flatten(inp), K.flatten(outp))
        #     return loss

        # def reconstruction_loss(inputs, outputs):
        #     loss = 0.
        #     for inp, outp in zip(inputs, outputs):
        #         loss += reconstruction_loss_item(inp, outp)
        #     return loss / len(inputs)

        input_images  = [input_image_r, input_image_p, input_image_n]
        output_images = [reconstr_r, reconstr_p, reconstr_n]
        # recon_loss = reconstruction_loss(input_images, output_images)

        # ## KL loss ##
        # def kl_divergence_item(mean, logvar):
        #     import keras.backend as K
        #     loss = -0.5 * K.sum(1 + logvar - K.square(mean) - K.exp(logvar), axis=-1)
        #     return loss

        # def kl_divergence(means, logvars):
        #     loss = 0.
        #     for mean, logvar in zip(means, logvars):
        #         loss += kl_divergence_item(mean, logvar)
        #     return loss / len(means)

        means   = [encoding_r, encoding_p, encoding_n]
        logvars = [encoding_logvar_r, encoding_logvar_p, encoding_logvar_n]
        # kl_loss = kl_divergence(means, logvars)

        recon_loss = 0.
        h, w = self.input_shape[0], self.input_shape[1]
        for X, reconstruction in zip(input_images, output_images):
            # reconstruction loss
            if self.reconstruction_loss_crossentropy:
                recon_loss += h * w * metrics.binary_crossentropy(K.flatten(X), K.flatten(reconstruction))
            else: # mean square error
                recon_loss += h * w * metrics.mse(K.flatten(X), K.flatten(reconstruction))
        recon_loss /= len(input_images) # divide by 3 to compare to betaVAE loss

        kl_loss = 0.
        for E_mean, E_logvar in zip(means, logvars):
            # KL divergence
            kl_loss += K.mean(- 0.5 * K.sum(1 + E_logvar - K.square(E_mean) - K.exp(E_logvar), axis=-1))
        kl_loss /= len(means) # divide by 3 to compare to betaVAE loss

        ## Triplet loss ##
        def triplet_loss(r, p, n):
            l2_dist  = lambda x, y : K.sqrt(K.sum(K.square(x - y)))
            d_rp = l2_dist(r,p) # distance between reference and positive (to be minimized)
            d_rn = l2_dist(r,n) # distance between reference and negative (to be maximized)
            return K.mean(K.maximum(K.constant(0.0), self.margin + K.square(d_rp) - K.square(d_rn)))

        trip_loss = triplet_loss(*means)

        triplet_vae_loss = recon_loss \
                            + self.beta  * kl_loss \
                            + self.gamma * trip_loss

        # add loss to model and compile
        model.add_loss(triplet_vae_loss)
        model.compile(optimizer=self.optimizer)
        # add metrics, they are ignored when using add_loss and compile(metrics=]...[]). see workaround: https://github.com/keras-team/keras/issues/9459#issuecomment-469282443
        model.metrics_tensors.append(recon_loss)
        model.metrics_names.append("recon loss")
        model.metrics_tensors.append(self.beta * kl_loss)
        model.metrics_names.append("KL loss")
        model.metrics_tensors.append(self.gamma * trip_loss)
        model.metrics_names.append("triplet loss")

        print(f'Triplet VAE model: {model.count_params():,} params')
        return model

    def fit(self, generator, steps_per_epoch, epochs, **kwargs):
        self.history = self.model.fit_generator(generator=generator,
                                                steps_per_epoch=steps_per_epoch,
                                                epochs=epochs,
                                                **kwargs)
        self.plot_history()

    def save_weights(self, output_folder=None):
        from utils import save_weights
        save_weights(model=self.encoder, model_name='TripletVAE', filename='encoder.h5')
        save_weights(model=self.decoder, model_name='TripletVAE', filename='decoder.h5')

    def load_weights(self, output_folder=None, model_name='TripletVAE'):
        from utils import load_weights
        load_weights(model=self.encoder, model_name=model_name, filename='encoder.h5')
        load_weights(model=self.decoder, model_name=model_name, filename='decoder.h5')



    def transform(self, images):
        '''Transform images to latent embedding vector using encoder

        Args:
            images: array size (N, H, W, C)
                input images to transform
        Returns:
            array size (N, latent_dim)
                encoding / embedding for each input image
        '''
        return self.encoder.predict(images)[0]

    def inverse_transform(self, latent_vector):
        '''Reconstruct image from latent vector using decoder

        Args:
            latent_vector: array size (N, latent_dim)
                latent vector to reconstruct
        Returns:
            output: array (N, H, W, C)
                zero-cropped reconstructions
        '''
        return self.decoder.predict(latent_vector)

    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    def plot(self, shapes=False, names=False, horizontal=False):
        from keras.utils.vis_utils import model_to_dot
        return model_to_dot(self.model, show_shapes=shapes, show_layer_names=names, rankdir='LR' if horizontal else 'TB').create(prog='dot', format='svg')
        from IPython.display import SVG, display
        display(SVG(viz.plot_network(self.model, names=True, horizontal=True)))

    def plot_history(self, start=0, end=None):
        if self.history is None or len(self.history.history) == 0:
            warnings.warn('No history found. Please run fit() first.')
        else:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots(4,1,figsize=(10,10), sharex=True)
            plt.subplots_adjust(hspace=.0)
            line_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

            # get list of losses and metrics
            metrics = list(self.history.history.keys())
            x = self.history.epoch[start:end]
            # plot total loss
            ax[0].plot(x, self.history.history['loss'][start:end], label='loss', lw=3, color='k')
            # plot all other losses/metrics
            for i, metric in enumerate(metrics[1:]):
                ax[0].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            # plot all losses/metrics in single plot
            for i, metric in enumerate(metrics[1:]):
                ax[i+1].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            for a in ax: a.legend()
            ax[-1].set_xlabel('Epochs')