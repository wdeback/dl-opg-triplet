import numpy as np
import os.path as osp
import pandas as pd
import keras

# -=-=-=-=-=- GPU memory -=-=-=-=-=-

def limit_GPU_memory():
    import tensorflow as tf
    from keras.backend.tensorflow_backend import set_session
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = True  # to log device placement (on which device the operation ran)
    sess = tf.Session(config=config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras

# -=-=-=-=-=- save and load model weights -=-=-=-=-=-

def save_weights(model:keras.models.Model, model_name:str, filename:str, folder:str='../output/models'):
    '''Save model weights to file.

    Args:
        model:keras.models.Model
            keras model
        model_name:str
            name of model, e.g. 'TripletCNN'
        filename:str
            filename, e.g. 'encoder.h5'
        folder:str
            folder, e.g. '../output/models'
    '''
    import os
    output_folder = os.path.join(folder, model_name)
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    out_fn = os.path.join(output_folder, filename)
    model.save_weights(out_fn)
    print(f'Weights of {model_name} saved to {out_fn}...')

def load_weights(model:keras.models.Model, model_name:str, filename:str, folder:str='../output/models'):
    '''Load model weights from file.

    Args:
        model:keras.models.Model
            keras model
        model_name:str
            name of model, e.g. 'TripletCNN'
        filename:str
            filename, e.g. 'encoder.h5'
        folder:str
            folder, e.g. '../output/models'
    '''
    import os
    input_folder = os.path.join(folder, model_name)
    in_fn = os.path.join(input_folder, filename)
    if os.path.exists(in_fn):
        model.load_weights(in_fn)
        print(f'Weights loaded from {in_fn}.')
    else:
        warnings.warn(f'Weights file {in_fn} not found.')


# -=-=-=-=-=- image reading -=-=-=-=-=-

def read_image(path, h, w, image_folder=None):
    from imageio import imread
    from skimage.transform import resize

    if path.endswith('.tif'):
        path = path[:-4]+'.png'
    if osp.dirname(path) == '':
        if image_folder is None:
            raise ValueError('If path does not contain folder name, `image_folder` must be specified.')
        path = osp.join(image_folder, path)

    try:
        img = imread(path).astype(np.float32)
    except:
        return None

    if img.ndim != 2:
        warnings.warn(f'Ignoring image with shape {img.shape}')
        return None
    # normalize
    img /= 255.
    # resize
    img = resize(img, (h,w))
    # add color channel
    img = img[..., np.newaxis]
    return img

def read_images(paths, h, w, image_folder, progressbar=True):

    from joblib import Parallel, delayed
    if progressbar:
        from tqdm import tqdm_notebook as tqdm
        images = Parallel(n_jobs=-1)(delayed(read_image)(path, h=h, w=w, image_folder=image_folder) for path in tqdm(paths, total=len(paths)))
    else:
        images = Parallel(n_jobs=-1)(delayed(read_image)(path, h=h, w=w, image_folder=image_folder) for path in paths)
    images = list(filter(None.__ne__, images))
    images = np.array(images)
    return images

# -=-=-=-=-=- image reading -=-=-=-=-=-


# -=-=-=-=-=- image reconstruction -=-=-=-=-=-

def get_random_image(df, imsize, data_folder):
    # get random sample
    query = df.sample(n=1)
    query_image_fn = query['Filename'].values[0]
    # read image
    from utils import read_image
    im = read_image(query_image_fn, h=imsize[0], w=imsize[1], image_folder=data_folder)
    return im

def reconstruct(model, im):
    if not hasattr(model, 'decoder'):
        raise ValueError(f'Cannot reconstruct image: model does not have an `decoder` attribute.')

    # get encoding z using encoder
    output = model.encoder.predict(im[np.newaxis,...])
    mean = output[0][0]
    logvar = output[1][0]
    z = output[2][0]
    # reconstruct image using decoder
    im_recon = model.decoder.predict(z[np.newaxis,...])
    return im_recon, mean, logvar

def plot_reconstruction(im, mean, logvar, im_recon, plot_std):
    #plt.bar(list(range(len(z))), z, yerr=np.exp(logvar), align='center', alpha=0.5, ecolor='black', capsize=10)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,3, figsize=(20,3))

    ax[0].imshow(np.squeeze(im), cmap='gray')
    ax[0].set_title(f'Original (min = {im.min():.2f} max = {im.max():.2f})')
    ax[0].axis('off')

    x = list(range(len(mean)))
    y = mean
    ax[1].bar(x, y, align='center', alpha=0.9, ecolor='black', capsize=10)
    if plot_std:
        err = np.exp(logvar)
        #err = np.exp(logvar)
        ax[1].errorbar(x, y, yerr=err, fmt='.', color='k', alpha=0.2)
    ax[1].set_xlabel('Latents')
    ax[1].set_title('Mean vector')

    ax[2].imshow(np.squeeze(im_recon), cmap='gray')
    ax[2].set_title(f'Reconstruction (min = {im_recon.min():.2f} max = {im_recon.max():.2f})')
    ax[2].axis('off')

    return fig

def reconstruct_random_image(df:pd.DataFrame, model, imsize, data_folder, plot_std=False):
    im = get_random_image(df, imsize, data_folder)
    im_recon, mean, logvar = reconstruct(model, im)
    fig = plot_reconstruction(im, mean, logvar, im_recon, plot_std=plot_std)
    return fig

# -=-=-=-=-=- end image reconstruction -=-=-=-=-=-
