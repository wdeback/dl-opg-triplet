

def ssim_distance(im1, im2):
    '''Compute structural similarity (ssim) between two images.
    Based on tf.image.ssim: https://www.tensorflow.org/api_docs/python/tf/image/ssim'''
    from tf.image import ssim
    return ssim(im1, im2, max_val=1.0, filter_size=5)




# https://gist.github.com/duhaime/211365edaddf7ff89c0a36d9f3f7956c

import warnings
from skimage.measure import compare_ssim
from skimage.transform import resize
from scipy.misc import imsave
from scipy.ndimage import imread
import numpy as np
# import cv2

##
# Globals
##

warnings.filterwarnings('ignore')

# specify resized image sizes
#height = 2**10
#width = 2**10
height, width = (128, 256) # thumbnails

##
# Functions
##

def get_img(path, norm_size=True, norm_exposure=False):
  '''
  Prepare an image for image processing tasks
  '''
  # flatten returns a 2d grayscale array
  img = imread(path, flatten=True).astype(int)
  # resizing returns float vals 0:255; convert to ints for downstream tasks
  if norm_size:
    img = resize(img, (height, width), anti_aliasing=True, preserve_range=True)
  if norm_exposure:
    img = normalize_exposure(img)
  return img


def get_histogram(img):
  '''
  Get the histogram of an image. For an 8-bit, grayscale image, the
  histogram will be a 256 unit vector in which the nth value indicates
  the percent of the pixels in the image with the given darkness level.
  The histogram's values sum to 1.
  '''
  h, w = img.shape
  hist = [0.0] * 256
  for i in range(h):
    for j in range(w):
      hist[img[i, j]] += 1
  return np.array(hist) / (h * w)


def normalize_exposure(img):
  '''
  Normalize the exposure of an image.
  '''
  img = img.astype(int)
  hist = get_histogram(img)
  # get the sum of vals accumulated by each position in hist
  cdf = np.array([sum(hist[:i+1]) for i in range(len(hist))])
  # determine the normalization values for each unit of the cdf
  sk = np.uint8(255 * cdf)
  # normalize each position in the output image
  height, width = img.shape
  normalized = np.zeros_like(img)
  for i in range(0, height):
    for j in range(0, width):
      normalized[i, j] = sk[img[i, j]]
  return normalized.astype(int)


# def earth_movers_distance(a,b):
#   '''
#   Measure the Earth Mover's distance between two images
#   @args:
#     {str} path_a: the path to an image file
#     {str} path_b: the path to an image file
#   @returns:
#     TODO
#   '''
#   from scipy.stats import wasserstein_distance
#   if isinstance(a, str):
#     img_a = get_img(path_a, norm_exposure=True)
#   elif isinstance(a, np.ndarray):
#     img_a = normalize_exposure(a*255)
#   if isinstance(b, str):
#     img_b = get_img(path_b, norm_exposure=True)
#   elif isinstance(b, np.ndarray):
#     img_b = normalize_exposure(b*255)
#   hist_a = get_histogram(img_a)
#   hist_b = get_histogram(img_b)
#   return wasserstein_distance(hist_a, hist_b)*1e3

def earth_mover_distance(a, b, r=2):
    from keras import backend as K
    # flatten all but first dimension
    a = K.batch_flatten(a)
    b = K.batch_flatten(b)
    # compute cumulative sum
    cdf_a = K.cumsum(a, axis=-1)
    cdf_b = K.cumsum(b, axis=-1)
    # calculate EMD for each sample
    samplewise_emd = K.pow(K.mean(K.pow(K.abs(cdf_a - cdf_b), r), axis=-1), 1/r)
    return samplewise_emd

def ssim_distance(a, b, window_size=9):
  '''
  Measure the structural similarity between two images
  @args:
    {str} path_a: the path to an image file
    {str} path_b: the path to an image file
  @returns:
    {float} a float {-1:1} that measures structural similarity
      between the input images
  '''
  if isinstance(a, str):
    img_a = get_img(path_a, norm_exposure=False)
  elif isinstance(a, np.ndarray):
    img_a = a
  if isinstance(b, str):
    img_b = get_img(path_b, norm_exposure=False)
  elif isinstance(b, np.ndarray):
    img_b = b

  sim = compare_ssim(img_a, img_b, win_size=window_size, full=False)
  return 1.-sim


def pixel_distance(a, b):
  '''
  Measure the pixel-level similarity between two images
  @args:
    {str} path_a: the path to an image file
    {str} path_b: the path to an image file
  @returns:
    {float} a float {-1:1} that measures structural similarity
      between the input images
  '''
  if isinstance(a, str):
    img_a = get_img(path_a, norm_exposure=True)
  elif isinstance(a, np.ndarray):
    img_a = normalize_exposure(a)
  if isinstance(b, str):
    img_b = get_img(path_b, norm_exposure=True)
  elif isinstance(b, np.ndarray):
    img_b = normalize_exposure(b)
  return np.sum(np.absolute(img_a - img_b)) / (height*width) / 255


#  def sift_sim(path_a, path_b):
#    '''
#    Use SIFT features to measure image similarity
#    @args:
#      {str} path_a: the path to an image file
#      {str} path_b: the path to an image file
#    @returns:
#      TODO
#    '''
#     # initialize the sift feature detector
#     orb = cv2.ORB_create()

#     # get the images
#     img_a = cv2.imread(path_a)
#     img_b = cv2.imread(path_b)

#     # find the keypoints and descriptors with SIFT
#     kp_a, desc_a = orb.detectAndCompute(img_a, None)
#     kp_b, desc_b = orb.detectAndCompute(img_b, None)

#     # initialize the bruteforce matcher
#     bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

#     # match.distance is a float between {0:100} - lower means more similar
#     matches = bf.match(desc_a, desc_b)
#     similar_regions = [i for i in matches if i.distance < 70]
#     if len(matches) == 0:
#         return 0
#     return len(similar_regions) / len(matches)

def set_keras_memory_session():
    from keras.backend.tensorflow_backend import set_session
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = True  # to log device placement (on which device the operation ran)
    sess = tf.Session(config=config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras

def cae_encoding(a, cae='../../output/models/encoder.h5'):
    '''
    Compute encoding using CAE.
    '''

    if isinstance(a, str):
        try:
            img_a = get_img(a, norm_exposure=False)
        except:
            return None
    elif isinstance(a, np.ndarray):
        img_a = normalize_exposure(a)

    if isinstance(cae, str):
        from keras.models import load_model
        set_keras_memory_session()
        print(os.pwd)
        print(cae)
        encoder = load_model(cae)
    else:
        encoder = cae.encoder

    img_a = np.expand_dims(np.expand_dims(img_a, 0), -1)
    encoding_a = encoder.predict(img_a).flatten()

    return encoding_a


def cae_distance(a, b, cae='../../output/models/encoder.h5'):
    '''
    Use a pre-trained convolutional autoencoder to estimate image dissimilarity.
    '''
    if isinstance(a, str):
        img_a = get_img(a, norm_exposure=False)
    elif isinstance(a, np.ndarray):
        img_a = normalize_exposure(a)
    if isinstance(b, str):
        img_b = get_img(a, norm_exposure=False)
    elif isinstance(b, np.ndarray):
        img_b = normalize_exposure(b)

    if isinstance(cae, str):
        from keras.models import load_model
        set_keras_memory_session()
        encoder = load_model(cae)
    else:
        encoder = cae.encoder

    img_a = np.expand_dims(np.expand_dims(img_a, 0), -1)
    encoding_a = encoder.predict(img_a).flatten()
    img_b = np.expand_dims(np.expand_dims(img_b, 0), -1)
    encoding_b = encoder.predict(img_b).flatten()

    # euclidea distance between encodings
    return np.linalg.norm(encoding_a-encoding_b)
