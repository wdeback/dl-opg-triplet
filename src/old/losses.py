
import keras.backend as K
cos_dist = lambda x, y : K.sum(x * y)/K.sqrt(K.sum(x ** 2))/K.sqrt(K.sum(y ** 2))
l2_dist  = lambda x, y : K.sqrt(K.sum(K.square(x - y)))
l1_dist  = lambda x, y : K.sum(K.abs(x - y))

import tensorflow as tf
def huber_loss(y_true, y_pred): # a.k.a. smooth_l1 loss
    return tf.losses.huber_loss(y_true, y_pred, delta=1.0)

def triplet_loss(margin):
    '''Triplet loss.
    
    Args:
    - margin (float): separation of clusters
    - y_true: ignored
    - y_pred: predicted vector with shape (N, V, 3) where N=batch_size, V=vector size.
    '''
    
    def loss(y_true, y_pred):
        import keras.backend as K
        length = K.int_shape(y_pred)[-1] // 3

        r = y_pred[:,0*length:1*length]
        p = y_pred[:,1*length:2*length]
        n = y_pred[:,2*length:3*length]
        #r = y_pred[...,0] # reference
        #p = y_pred[...,1] # positive
        #n = y_pred[...,2] # negative
        #d1 = l1_dist(r,p) # distance between reference and positive (to be minimized) 
        #d2 = l1_dist(r,n) # distance between reference and negative (to be maximized)
        d1 = l2_dist(r,p) # distance between reference and positive (to be minimized) 
        d2 = l2_dist(r,n) # distance between reference and negative (to be maximized)
        #d1 = huber_loss(r,p) # distance between reference and positive (to be minimized) 
        #d2 = huber_loss(r,n) # distance between reference and negative (to be maximized)
        margin = 10.0

        #maxval = 100000.0
        #d1 = K.clip(d1, 0., maxval)
        #d2 = K.clip(d2, 0., maxval)

        return K.mean(K.maximum(K.constant(0.0), margin + K.square(d1) - K.square(d2)))
    return loss


def vae_loss(beta:float, reconstruction_loss:string='mse') -> float:
    '''Loss function for beta-VAE
    
    Args:
    - beta: 
        strength of KL divergence loss
    - reconstruction_loss:
        'mse' or 'xentropy'
        
    '''

    def loss(y_true, y_pred):
        '''beta VAE'''
        # Compute VAE loss
        # apparently, the loss needs to be in a function (not via add_loss) if using fit_generator()
        # https://github.com/keras-team/keras/issues/10137
        kl_loss = - 0.5 * K.sum(1 + E_logsigma - K.square(E_mean) - K.exp(E_logsigma), axis=-1)
        if reconstruction_loss == 'mse':
            rec_loss = self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(output))
        else: #if reconstruction_loss == 'xentropy':
            rec_loss = self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(output))
        vaeloss = K.mean(rec_loss + self.beta * kl_loss)
        return vaeloss
    return loss
