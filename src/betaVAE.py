import numpy as np
from keras import layers, models
from keras import metrics
from keras import backend as K
from utils import limit_GPU_memory
import warnings


class BetaVAE():

    def __init__(self, input_shape:tuple,
                        latent_dim:int=64,
                        beta:float=10.0,
                        recon_loss:str='bce',
                        n_layers:int=3,
                        n_filters:int=32,
                        kernel_size:int=3,
                        optimizer:str='nadam',
                        compile:bool=True):
        self.rows, self.columns, self.channels = input_shape
        self.beta = beta  # 10.0
        self.n_layers = n_layers
        self.n_filters = n_filters
        self.kernel_size = kernel_size
        self.latent_dim = latent_dim
        valid_recon_losses = ['bce', 'xe', 'crossentropy', 'mse', 'mean_square_error']
        if recon_loss not in valid_recon_losses:
            warnings.warn(f'recon_loss must be one of: {valid_recon_losses}')
        elif recon_loss in ['bce', 'xe', 'crossentropy']:
            self.reconstruction_loss_crossentropy = True # crossentropy
        else:
            self.reconstruction_loss_crossentropy = False # MSE
        self.optimizer = optimizer
        limit_GPU_memory()  # allow VRAM growth
        self.model = self.build_model(compile=compile)
        #self.plot(shapes=True)
        self.history = None


    def build_model(self, compile=False):

        # encoder
        print('encoder')
        self.encoder, shape = self.build_encoder(kernel_size=self.kernel_size, n_layers=self.n_layers)

        # decoder
        print('decoder')
        self.decoder = self.build_decoder(kernel_size=self.kernel_size, shape=shape, n_layers=self.n_layers)

        # VAE
        print('vae')
        X = layers.Input(shape=(self.rows, self.columns, self.channels), name='input_image_VAE')
        E_mean, E_logvar, Z = self.encoder(X) # encoder
        reconstruction = self.decoder(Z) # decoder
        model = models.Model(inputs=X, outputs=[E_mean, E_logvar, reconstruction], name='VAE')

        # if using betaVAE as a submodel (e.g. from tripletVAE), do not compile model
        if not compile:
            return model

        # VAE loss = KL-divergence + MSE reconstruction loss

        ## Reconstruction loss (MSE or BCE) ##
        # def reconstruction_loss_item(inp, outp):
        #     h, w = self.rows, self.columns
        #     if self.reconstruction_loss_crossentropy:
        #         loss = h * w * metrics.binary_crossentropy(K.flatten(inp), K.flatten(outp))
        #     else: # mean square error
        #         loss = h * w * metrics.mse(K.flatten(inp), K.flatten(outp))
        #     return loss

        # def reconstruction_loss(inputs, outputs):
        #     loss = 0.
        #     for inp, outp in zip(inputs, outputs):
        #         loss += reconstruction_loss_item(inp, outp)
        #     return loss / len(inputs)

        # recon_loss = reconstruction_loss([X], [reconstruction])

        # ## KL loss ##
        # def kl_divergence_item(mean, logvar):
        #     import keras.backend as K
        #     loss = -0.5 * K.sum(1 + logvar - K.square(mean) - K.exp(logvar), axis=-1)
        #     return loss

        # def kl_divergence(means, logvars):
        #     loss = 0.
        #     for mean, logvar in zip(means, logvars):
        #         loss += kl_divergence_item(mean, logvar)
        #     return loss / len(means)

        # kl_loss = kl_divergence([E_mean], [E_logvar])

        # vae_loss = recon_loss + self.beta * kl_loss

        # KL divergence
        kl_loss = K.mean(- 0.5 * K.sum(1 + E_logvar - K.square(E_mean) - K.exp(E_logvar), axis=-1))

        # reconstruction loss
        if self.reconstruction_loss_crossentropy:
            recon_loss = self.rows * self.columns * metrics.binary_crossentropy(K.flatten(X), K.flatten(reconstruction))
        else: # mean square error
            recon_loss = self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(reconstruction))

        vae_loss = K.mean( recon_loss + self.beta * kl_loss )

        # kl_loss = - 0.5 * K.sum(1 + E_logvar - K.square(E_mean) - K.exp(E_logvar), axis=-1)
        # recon_loss =  self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(reconstruction))
        # vae_loss = K.mean( recon_loss + self.beta * kl_loss )

        # compile model
        model.add_loss(vae_loss)
        model.compile(optimizer=self.optimizer)
        # add metrics
        model.metrics_tensors.append(recon_loss)
        model.metrics_names.append("recon loss")
        model.metrics_tensors.append(self.beta * kl_loss)
        model.metrics_names.append("KL loss")

        print(f'Beta VAE model: {model.count_params():,} params')
        return model


    def build_encoder(self, kernel_size, n_layers):
        inp = layers.Input(shape=(self.rows, self.columns, self.channels), name='input_image_encoder')
        x = layers.Conv2D(filters=self.n_filters, kernel_size=self.kernel_size, strides=2, padding='same')(inp)
        x = layers.BatchNormalization(epsilon=1e-5)(x)
        x = layers.LeakyReLU(alpha=0.2)(x)

        for i in range(n_layers-1):
            self.n_filters *= 2
            x = layers.Conv2D(filters=self.n_filters, kernel_size=self.kernel_size, strides=2, padding='same')(x)
            x = layers.BatchNormalization(epsilon=1e-5)(x)
            x = layers.LeakyReLU(alpha=0.2)(x)

        shape = K.int_shape(x)
        x = layers.Flatten()(x)

        mean = layers.Dense(self.latent_dim, name='mean_vector')(x)
        logvar = layers.Dense(self.latent_dim, activation='tanh', name='logvar_vector')(x)
        z = layers.Lambda(self.sampling, output_shape=(self.latent_dim,))([mean, logvar])
        meansigma = models.Model([inp], [mean, logvar, z])
        return meansigma, shape

    def build_decoder(self, kernel_size, shape, n_layers):
        inp = layers.Input(shape=(self.latent_dim,), name='input_vector_decoder')
        x = layers.Dense(shape[1]*shape[2]*shape[3], activation='relu')(inp)
        x = layers.Reshape((shape[1],shape[2],shape[3]))(x)
        x = layers.BatchNormalization(epsilon=1e-5)(x)
        x = layers.Activation('relu')(x)

        for i in range(n_layers-1):
            self.n_filters //= 2
            x = layers.Conv2DTranspose(filters=self.n_filters, kernel_size=self.kernel_size, strides=2, padding='same')(x)
            x = layers.BatchNormalization(epsilon=1e-5)(x)
            x = layers.Activation('relu')(x)

        x = layers.Conv2DTranspose(filters=self.channels, kernel_size=self.kernel_size, strides=2, padding='same')(x)
        o = layers.Activation('sigmoid', name='reconstructed_image')(x)

        model = models.Model(inp, o)
        return model

    def sampling(self, args):
        """Reparameterization trick by sampling fr an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """
        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean=0 and std=1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon

    def fit(self, generator, steps_per_epoch, epochs, **kwargs):
        self.history = self.model.fit_generator(generator=generator,
                                           steps_per_epoch=steps_per_epoch,
                                           epochs=epochs,
                                           **kwargs)
        self.plot_history()

    def save_weights(self, output_folder=None):
        from utils import save_weights
        save_weights(model=self.encoder, model_name='BetaVAE', filename='encoder.h5')
        save_weights(model=self.decoder, model_name='BetaVAE', filename='decoder.h5')

    def load_weights(self, output_folder=None):
        from utils import load_weights
        load_weights(model=self.encoder, model_name='BetaVAE', filename='encoder.h5')
        load_weights(model=self.decoder, model_name='BetaVAE', filename='decoder.h5')


    def transform(self, images):
        '''Transform images to latent embedding vector using encoder

        Args:
            images: array size (N, H, W, C)
                input images to transform
        Returns:
            array size (N, latent_dim)
                encoding / embedding for each input image
        '''
        return self.encoder.predict(images)[0]

    def inverse_transform(self, latent_vector):
        '''Reconstruct image from latent vector using decoder

        Args:
            latent_vector: array size (N, latent_dim)
                latent vector to reconstruct
        Returns:
            output: array (N, H, W, C)
                zero-cropped reconstructions
        '''
        return self.decoder.predict(latent_vector)

    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    def plot(self, shapes=False, names=False, horizontal=False):
        from keras.utils.vis_utils import model_to_dot
        from IPython.display import SVG, display
        display(SVG(model_to_dot(self.model, show_shapes=shapes, show_layer_names=names, rankdir='LR' if horizontal else 'TB').create(prog='dot', format='svg')))
        if self.encoder:
            display(SVG(model_to_dot(self.encoder, show_shapes=shapes, show_layer_names=names, rankdir='LR' if horizontal else 'TB').create(prog='dot', format='svg')))
        if self.decoder:
            display(SVG(model_to_dot(self.decoder, show_shapes=shapes, show_layer_names=names, rankdir='LR' if horizontal else 'TB').create(prog='dot', format='svg')))


    def plot_history(self, start=0, end=None):
        if self.history is None or len(self.history.history) == 0:
            warnings.warn('No history found. Please run fit() first.')
        else:
            import matplotlib.pyplot as plt
            metrics = list(self.history.history.keys())
            fig, ax = plt.subplots(len(metrics),1,figsize=(10,10), sharex=True)
            plt.subplots_adjust(hspace=.0)
            line_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

            # get list of losses and metrics
            x = self.history.epoch[start:end]
            # plot total loss
            ax[0].plot(x, self.history.history['loss'][start:end], label='loss', lw=3, color='k')
            # plot all other losses/metrics
            for i, metric in enumerate(metrics[1:]):
                ax[0].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            # plot all losses/metrics in single plot
            for i, metric in enumerate(metrics[1:]):
                ax[i+1].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            for a in ax: a.legend()
            ax[-1].set_xlabel('Epochs')



# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

class VAE():
    '''Variational Auto-Encoder (VAE)'''

    def __init__(self, latent_dim:int,
                        x_train:np.ndarray,
                        x_validation:np.ndarray,
                        epochs:int=500,
                        beta:float=2.0,
                        batch_size:int=32,
                        validation:float=0.1,
                        filters:int=12,
                        verbose:bool=True):
        '''
        Variational autoencoder.

        Args:
            latent_dim (int):
                length of latent vector
            x_train (ndarray):
                data as ndarray
            x_validation (ndarray):
                data as ndarray
            epochs (int):
                number of epochs to train, unless stopped by early stopping callback
            beta (float):
                beta value to increase weight of KL loss (beta=1.0: normal VAE, beta>1.0: beta-VAE)
            batch_size (int):
                batch size
            validation (float):
                fraction of data to use as validation set
            filters (int):
                number of convolutional filters/kernels to use in first/last layers
            verbose (bool):
                verbosity level
        '''
        self.x_train = x_train
        self.x_validation = x_validation

        self.latent_dim = latent_dim

        print('set sizes')

        # get sizes etc.
        _, self.rows, self.columns, self.channels = self.x_train.shape
        self.batch_size = batch_size
        self.filters = filters
        self.beta = beta  # beta-VAE: controls weight of KL loss
        self.epochs = epochs
        self.verbose = verbose
        self.validation_split = 0.1
        self.callbacks = []
        self.initialize()

    def initialize(self):

        print('initialize')

        # set allow_growth GPU memory option
        self.limit_GPU_memory()

        # optimizers
        from keras.optimizers import Adam
        optimizer = Adam(lr=0.0002)

        print('encoder')

        # encoder
        self.E, shape = self.encoder(kernel_size=3)
        self.E.compile(optimizer=optimizer)

        print('decoder ')

        # decoder
        self.D = self.decoder(kernel_size=3, shape=shape)
        self.D.compile(optimizer=optimizer)

        print('vae')

        # VAE
        X = layers.Input(shape=(self.rows, self.columns, self.channels))
        # latent_rep = E(X)[0]
        # output = D(latent_rep)
        E_mean, E_logsigma, Z = self.E(X) # encoder
        # Z = Input(shape=(self.latent_dim),))
        # Z2 = Input(shape=(batch_size, self.latent_dim))
        output = self.D(Z) # decoder
        self.AE = models.Model(X, output, name='VAE')

#         if self.verbose:
#             self.E.summary()
#             self.D.summary()
#             self.AE.summary()

        print('loss')

        # VAE loss = KL-divergence + MSE reconstruction loss
        kl_loss = - 0.5 * K.sum(1 + E_logsigma - K.square(E_mean) - K.exp(E_logsigma), axis=-1)
        reconstruction_loss =  self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(output))
        VAEloss = K.mean( reconstruction_loss + self.beta * kl_loss )
        self.AE.add_loss(VAEloss)
        self.AE.compile(optimizer=optimizer)

        # Compute VAE loss
        ## apparently, the loss needs to be in a function (not via add_loss) if using fit_generator()
        ## https://github.com/keras-team/keras/issues/10137
        #def vae_loss(y_true, y_pred):
        #    kl_loss = - 0.5 * K.sum(1 + E_logsigma - K.square(E_mean) - K.exp(E_logsigma), axis=-1)
        #    mse_loss = self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(output))
        #    vaeloss = K.mean(mse_loss + self.beta * kl_loss)
        #    return vaeloss
        #self.AE.compile(optimizer=optimizer, loss=vae_loss)

        print('callbacks')

        # callbacks
        self.callbacks.append(
            callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=20, verbose=0,
                                    mode='min', baseline=None, restore_best_weights=False))


    def fit(self, plot:bool=True):
        print('fit')
        self.history = self.AE.fit(self.x_train,
                    epochs=self.epochs,
                    batch_size=self.batch_size,
                    validation_data=(self.x_validation, None),
                    verbose=self.verbose,
                    callbacks=self.callbacks)

        if plot:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots(1,1)
            ax.plot(self.history.history['loss'], label='loss')
            #if 'val_loss' in self.history.history.keys():
            ax.plot(self.history.history['val_loss'], label='val_loss')
            plt.legend()


    def transform(self, X):
        '''Compute latent vectors from inputs X'''
        return self.E.predict(X)[0]

    #TODO: test inverse transform
    def inverse_transform(self, X):
        '''Reconstruct from latent vector

        Args:
            X (ndarray, size = (None, VAE.latent_dim)): latent vectors
        Returns:
            output (ndarray): zero-cropped reconstructions, un-normalized to max value
        '''
        return self.crop(self.D.predict(X)) * 255.

    def sampling(self, args):
        """Reparameterization trick by sampling fr an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """
        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean=0 and std=1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon

    def encoder(self, kernel_size):
        inp = layers.Input(shape=(self.rows, self.columns, self.channels))
        x = layers.Conv2D(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(inp)
        x = layers.BatchNormalization(epsilon=1e-5)(x)
        x = layers.LeakyReLU(alpha=0.2)(x)

        for i in range(2):
            self.filters *= 2
            x = layers.Conv2D(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(x)
            x = layers.BatchNormalization(epsilon=1e-5)(x)
            x = layers.LeakyReLU(alpha=0.2)(x)

        shape = K.int_shape(x)
        x = layers.Flatten()(x)

        mean = layers.Dense(self.latent_dim)(x)
        logsigma = layers.Dense(self.latent_dim, activation='tanh')(x)
        latent = layers.Lambda(self.sampling, output_shape=(self.latent_dim,))([mean, logsigma])
        meansigma = models.Model([inp], [mean, logsigma, latent])
        return meansigma, shape


    def decoder(self, kernel_size, shape):
        inp = layers.Input(shape=(self.latent_dim,))

        x = layers.Dense(shape[1]*shape[2]*shape[3])(inp)
        x = layers.Reshape((shape[1],shape[2],shape[3]))(x)
        x = layers.BatchNormalization(epsilon=1e-5)(x)
        x = layers.Activation('relu')(x)

        for i in range(2):
            x = layers.Conv2DTranspose(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(x)
            x = layers.BatchNormalization(epsilon=1e-5)(x)
            x = layers.Activation('relu')(x)
            self.filters //= 2

        x = layers.Conv2DTranspose(filters=self.channels, kernel_size=kernel_size, strides=2, padding='same')(x)
        o = layers.Activation('tanh')(x)

        model = models.Model(inp, o)
        return model

    def crop(self, arr):
        '''crop zeros from numpy array outside of bounding box
        https://stackoverflow.com/a/39466129'''

        # argwhere will give you the coordinates of every non-zero point
        coords = np.argwhere(arr)
        # take the smallest points and use them as the top left of your crop
        x_min, y_min, z_min = coords.min(axis=0)
        x_max, y_max, z_max = coords.max(axis=0)
        # crop array
        out = arr[x_min:x_max+1,
                  y_min:y_max+1,
                  z_min:z_max+1] # plus 1 because slice isn't
        return out
