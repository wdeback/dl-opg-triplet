from keras import backend as K
from utils import limit_GPU_memory
import numpy as np
import warnings

class TripletCNN(object):

    def __init__(self,
                 input_shape:tuple,
                 latent_dim:int=64,
                 recon_loss:str='bce',
                 margin:float=10.0,
                 optimizer:str='nadam',
                 n_filters:int=32,
                 n_layers:int=3,
                 kernel_size:int=3):
        '''Triplet CNN network.

        Args:
            input_shape: tuple
                shape of input images, excluding batch size (H,W,C)
            latent_dim: int
                length of latent vector
            margin: float:
                margin (hinge) in triplet loss
            optimizer: str
                SGD optimizer
        '''
        self.input_shape = input_shape
        self.rows, self.columns, self.channels = self.input_shape
        self.latent_dim = latent_dim
        self.margin = margin
        self.n_filters = n_filters
        self.n_layers = n_layers
        self.kernel_size = kernel_size

        valid_recon_losses = ['bce', 'xe', 'crossentropy', 'mse', 'mean_square_error']
        if recon_loss not in valid_recon_losses:
            warnings.warn(f'recon_loss must be one of: {valid_recon_losses}')
        elif recon_loss in ['bce', 'xe', 'crossentropy']:
            self.reconstruction_loss_crossentropy = True # crossentropy
        else:
            self.reconstruction_loss_crossentropy = False # MSE
        self.optimizer = optimizer
        self.submodel = None
        limit_GPU_memory()  # allow VRAM growth
        self.model = self.build_model()
        self.plot()
        self.history = None


    def get_encoder(self, n_layers:int, n_filters:int, kernel_size:int):
        from keras import layers, models
        inp = layers.Input(shape=(self.rows, self.columns, self.channels))
        x = layers.Conv2D(filters=self.n_filters, kernel_size=self.kernel_size, strides=2, padding='same')(inp)
        x = layers.BatchNormalization(epsilon=1e-5)(x)
        x = layers.LeakyReLU(alpha=0.2)(x)

        for i in range(n_layers-1):
            self.n_filters *= 2
            x = layers.Conv2D(filters=self.n_filters, kernel_size=self.kernel_size, strides=2, padding='same')(x)
            x = layers.BatchNormalization(epsilon=1e-5)(x)
            x = layers.LeakyReLU(alpha=0.2)(x)

        x = layers.Flatten()(x)
        from keras import regularizers
        output = layers.Dense(self.latent_dim, activity_regularizer=regularizers.l2(1e-4))(x)
        model = models.Model([inp], [output], name='CNN_model')
        return model

    def build_model(self):
        from keras import layers, models, metrics
        import keras.backend as K

        # create one CNN model
        self.encoder = self.get_encoder(n_layers=self.n_layers,
                                    n_filters=self.n_filters,
                                    kernel_size=self.kernel_size)

        # images
        input_image_r = layers.Input(shape=self.input_shape, name='reference_image')
        input_image_p = layers.Input(shape=self.input_shape, name='positive_image')
        input_image_n = layers.Input(shape=self.input_shape, name='negative_image')

        encoding_r = self.encoder(input_image_r)
        encoding_p = self.encoder(input_image_p)
        encoding_n = self.encoder(input_image_n)

        # mean encoding output
        output_encoding = layers.Concatenate(axis=-1)([encoding_r,
                                                       encoding_p,
                                                       encoding_n])
        output_encoding = layers.Activation('linear')(output_encoding)
        output_encoding = layers.Reshape( (-1,3) )(output_encoding)

        # model input/output
        model = models.Model(inputs=[input_image_r,
                                     input_image_p,
                                     input_image_n],
                                     outputs=[output_encoding],
                                     name='triplet_cnn_network')

        # LOSSES: triplet loss

        ## Triplet loss ##
        def triplet_loss(r, p, n):
            l2_dist  = lambda x, y : K.sqrt(K.sum(K.square(x - y)))
            d1 = l2_dist(r,p) # distance between reference and positive (to be minimized)
            d2 = l2_dist(r,n) # distance between reference and negative (to be maximized)
            return K.mean(K.maximum(K.constant(0.0), self.margin + K.square(d1) - K.square(d2)))

        trip_loss = triplet_loss(encoding_r, encoding_p, encoding_n)

        # add loss to model and compile
        model.add_loss(trip_loss)
        model.compile(optimizer=self.optimizer)
        # add metrics, they are ignored when using add_loss and compile(metrics=]...[]). see workaround: https://github.com/keras-team/keras/issues/9459#issuecomment-469282443
        model.metrics_tensors.append(trip_loss)
        model.metrics_names.append("triplet loss")

        print(f'Triplet CNN model: {model.count_params():,} params')
        return model

    def fit(self, generator, steps_per_epoch, epochs, **kwargs):
        self.history = self.model.fit_generator(generator=generator,
                                                steps_per_epoch=steps_per_epoch,
                                                epochs=epochs,
                                                **kwargs)
        self.plot_history()

    def save_weights(self, output_folder=None):
        from utils import save_weights
        save_weights(model=self.encoder, model_name='TripletCNN', filename='encoder.h5')

    def load_weights(self, output_folder=None):
        from utils import load_weights
        load_weights(model=self.encoder, model_name='TripletCNN', filename='encoder.h5')

    def transform(self, images):
        '''Transform images to latent embedding vector using encoder

        Args:
            images: array size (N, H, W, C)
                input images to transform
        Returns:
            array size (N, latent_dim)
                encoding / embedding for each input image
        '''
        return self.encoder.predict(images)

    def inverse_transform(self, latent_vector):
        '''Reconstruct image from latent vector using decoder

        Args:
            latent_vector: array size (N, latent_dim)
                latent vector to reconstruct
        Returns:
            output: array (N, H, W, C)
                zero-cropped reconstructions
        '''
        raise ValueError(f'Inverse transform is not available in Triplet CNN')

    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    def plot(self, shapes=False, names=False, horizontal=False):
        from keras.utils.vis_utils import model_to_dot
        from IPython.display import SVG, display
        return display(SVG(model_to_dot(self.model, show_shapes=shapes, show_layer_names=names, rankdir='LR' if horizontal else 'TB').create(prog='dot', format='svg')))

    def plot_history(self, start=0, end=None):
        if self.history is None or len(self.history.history) == 0:
            warnings.warn('No history found. Please run fit() first.')
        else:
            import matplotlib.pyplot as plt
            metrics = list(self.history.history.keys())
            fig, ax = plt.subplots(len(metrics),1,figsize=(10,5), sharex=True)
            plt.subplots_adjust(hspace=.0)
            line_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

            # get list of losses and metrics
            x = self.history.epoch[start:end]
            # plot total loss
            ax[0].plot(x, self.history.history['loss'][start:end], label='loss', lw=3, color='k')
            # plot all other losses/metrics
            for i, metric in enumerate(metrics[1:]):
                ax[0].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            # plot all losses/metrics in single plot
            for i, metric in enumerate(metrics[1:]):
                ax[i+1].plot(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            for a in ax: a.legend()
            ax[-1].set_xlabel('Epochs')